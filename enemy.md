# Enemy mods

### Scale enemy health by player count
In: `{} -> EnemyAI -> Start()`

Change from
```
if (PhotonNetwork.inRoom && PhotonNetwork.room.PlayerCount > 1)
		{
			this.Health *= 1.75f;
			this.TotalHealth *= 1.75f;
```
to

```
if (PhotonNetwork.inRoom && PhotonNetwork.room.PlayerCount > 1)
		{
			this.TotalHealth *= (float)PhotonNetwork.room.PlayerCount * 0.875f;
			this.Health = this.TotalHealth;
```