# VR DungeonKnight modding guide

## get dnSpy
https://github.com/dnSpyEx/dnSpy/releases

Modify `VR DungeonKnight_Data\Managed\Assembly-CSharp.dll` in place to be able to use the C# editor.

## Useful modifications
* [Network](network.md)
* [Enemies](enemy.md)