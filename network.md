# Networking mods

### Increase player limit
In: `{} -> MultiplayerRostrum -> ConnectWithName()`

Change `roomOptions.MaxPlayers = 2;` as you want. The `if (!this.OpenLobby)` case is private lobby. The else case is public lobby.

### Change nameserver
In: `{} -> NetworkingPeer -> GetNameServerAddress()`

Change the domain in return statement